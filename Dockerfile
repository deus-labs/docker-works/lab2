FROM alpine:3.17.3

RUN apk add --no-cache nginx && mkdir -p /run/nginx

EXPOSE 8080

COPY config.conf /etc/nginx/http.d/

CMD ["nginx", "-g", "daemon off;"]
